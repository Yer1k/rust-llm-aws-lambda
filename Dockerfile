# Stage 1: Build Rust application
FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

WORKDIR /usr/src/app
COPY . .

# Build the Rust application
RUN cargo lambda build --release --arm64

# Stage 2: Create the final image
FROM public.ecr.aws/lambda/provided:al2-arm64

WORKDIR /rust-llm-aws-lambda

# Copy built artifacts from the previous stage
COPY --from=builder /usr/src/app/target/ ./
COPY --from=builder /usr/src/app/src/bloom.bin ./lambda/rust-llm-aws-lambda/

# Set the entry point
ENTRYPOINT ["./lambda/rust-llm-aws-lambda/bootstrap"]
