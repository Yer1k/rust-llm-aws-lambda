# Rust LLM AWS Lambda

This is an example of how to dockerize a Rust LLM project and deploy it to AWS Lambda.

## Features
- Utilze [llm](https://github.com/rustformers/llm) to build a text generation function that can be deployed to AWS Lambda.
- Model in use is [BLOOM](https://huggingface.co/docs/transformers/model_doc/bloom)
- AWS Lambda deployment is done using [AWS CLI](https://aws.amazon.com/cli/)

## Example Usage
We want to automatically generate a text based on the input text. Here, for example, we use "Rust with AWS" as the input text.
Then we get:
```bash
curl "http://localhost:9000?text=Rust%20with%20AWS"
```

![Example](./screenshots/local.png)

OR using AWS lambda, the input text is passed as a query parameter `text` to the lambda function. The lambda function will generate a text based on the input text and return it as a response:
```bash
curl "https://gerupw4q2kyr3jaadmkyhzx7ni0ccphs.lambda-url.us-east-1.on.aws/?text=Rust%20with%20AWS"
```

OR accessing the lambda function through web browser:
https://gerupw4q2kyr3jaadmkyhzx7ni0ccphs.lambda-url.us-east-1.on.aws/?text=Rust%20with%20AWS

![lambda_link](./screenshots/lambda_link.png)

## AWS Lambda Function
Using AWS CLI, we can deploy the Rust LLM project to AWS Lambda. 

![ecr_push](./screenshots/ecr_push.png)

The lambda function is created using the following code:
```bash
docker build -t rust-llm-aws-lambda .
```
Then, deploy the lambda function:
```bash
docker tag rust-llm-aws-lambda:latest 309324599654.dkr.ecr.us-east-1.amazonaws.com/rust-llm-aws-lambda:latest
```

Finally, push the image to ECR:
```bash
docker push 309324599654.dkr.ecr.us-east-1.amazonaws.com/rust-llm-aws-lambda:latest
```

![ecr_image](./screenshots/ecr_image.png)

After the image is pushed to ECR, we can deploy the lambda function using AWS lambda by creating with container image:
![lambda_function](./screenshots/lambda_function.png)

Then, we need to set up the trigger for the lambda function. Here, we use API Gateway as the trigger:
![lambda_trigger](./screenshots/lambda_trigger.png)

Finally, we can access the lambda function by setting the function URL:
![function_url](./screenshots/function_url.png)


## Screenshots
- Local deployment:
![local](./screenshots/local.png)

- Model while running on local machine:
![lambda_watch](./screenshots/lambda_watch.png)

- AWS Lambda deployment:
![lambda_link](./screenshots/lambda_link.png)
![cloud_watch](./screenshots/cloud_watch.png)
